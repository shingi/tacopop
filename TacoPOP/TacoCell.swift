//
//  TacoCell.swift
//  TacoPOP
//
//  Created by Shingi Mutandwa on 11/09/2016.
//  Copyright © 2016 Mutandwa. All rights reserved.
//

import UIKit

class TacoCell: UICollectionViewCell, NibLoadableView, Shakeable {

    @IBOutlet weak var tacoImage: UIImageView!
    @IBOutlet weak var tacoLabel: UILabel!
    
    var taco: Taco!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(taco: Taco) {
        
        self.taco = taco
        self.tacoImage.image = UIImage(named: taco.proteinId.rawValue)
        self.tacoLabel.text = taco.productName
    }

}
