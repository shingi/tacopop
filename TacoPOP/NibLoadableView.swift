//
//  NibLoadableView.swift
//  TacoPOP
//
//  Created by Shingi Mutandwa on 11/09/2016.
//  Copyright © 2016 Mutandwa. All rights reserved.
//

import UIKit

protocol NibLoadableView: class {}

extension NibLoadableView where Self: UIView {
    
    static var nibName: String {
        return String(describing: self)
    }
}
