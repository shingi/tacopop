//
//  ReusableView.swift
//  TacoPOP
//
//  Created by Shingi Mutandwa on 11/09/2016.
//  Copyright © 2016 Mutandwa. All rights reserved.
//

import UIKit


protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
